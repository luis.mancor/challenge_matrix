import 'package:challenge_csti/core/exceptions.dart';
import 'package:challenge_csti/core/utils/utility.dart';
import 'package:flutter_test/flutter_test.dart';

void main() {
  group('Utility Tests', () {
    test('parseMatrix - Matrix valida', () {
      String validInput = '[[1, 2, 3], [4, 5, 6], [7, 8, 9]]';
      List<List<int>> expectedMatrix = [
        [1, 2, 3],
        [4, 5, 6],
        [7, 8, 9],
      ];

      var result = Utility.parseMatrix(validInput);
      expect(result, expectedMatrix);
    });

    test('parseMatrix - matrix de 1x1', () {
      String validInput = '[[1]]';
      List<List<int>> expectedMatrix = [
        [1],
      ];

      var result = Utility.parseMatrix(validInput);
      expect(result, expectedMatrix);
    });

    test('parseMatrix - Formato invalido de matrix', () {
      String invalidInput = '{1, 2, 3}';
      expect(
        () => Utility.parseMatrix(invalidInput),
        throwsA(isA<InvalidMatrixFormatException>()),
      );
    });

    test('parseMatrix - Formato invalido(faltan brackets)', () {
      String invalidInput = '1, 2, 3';
      expect(
        () => Utility.parseMatrix(invalidInput),
        throwsA(isA<InvalidMatrixFormatException>()),
      );
    });

    test('parseMatrix - Contenido invalido (Elementos no enteros)', () {
      String invalidInput = '[[1, 2, "a"], [4, 5, 6], [7, 8, 9]]';
      expect(
        () => Utility.parseMatrix(invalidInput),
        throwsA(isA<InvalidContentListException>()),
      );
    });

    test('parseMatrix - No es una matriz cuadrada', () {
      String invalidInput = '[[1, 2], [3, 4, 5]]';
      expect(
        () => Utility.parseMatrix(invalidInput),
        throwsA(isA<NotSquareMatrixException>()),
      );
    });

    test('parseMatrix - Input vacio', () {
      String emptyInput = '';
      expect(
        () => Utility.parseMatrix(emptyInput),
        throwsA(isA<InvalidMatrixFormatException>()),
      );
    });

    group('isValidBracketString', () {
      test('Combinacion de bracket validas', () {
        expect(Utility.isValidBracketString('[]'), true);
        expect(Utility.isValidBracketString('[[]]'), true);
        expect(Utility.isValidBracketString('[[[][]]]'), true);
      });

      test('Combinaciones invalidas de brackets', () {
        expect(Utility.isValidBracketString('['), false);
        expect(Utility.isValidBracketString(']'), false);
        expect(Utility.isValidBracketString('][[[]]'), false);
      });
    });
  });
}
