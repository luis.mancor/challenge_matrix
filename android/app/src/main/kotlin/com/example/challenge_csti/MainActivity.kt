package com.example.challenge_csti

import android.widget.Toast
import androidx.annotation.NonNull
import com.droidman.ktoasty.KToasty
import io.flutter.embedding.android.FlutterActivity
import io.flutter.embedding.engine.FlutterEngine
import io.flutter.plugin.common.MethodChannel

class MainActivity: FlutterActivity() {
    
    private val CHANNEL = "com.luismancor.kotlin.snackbar"
    override fun configureFlutterEngine(@NonNull flutterEngine: FlutterEngine){
        super.configureFlutterEngine(flutterEngine)
        MethodChannel(flutterEngine.dartExecutor.binaryMessenger, CHANNEL).setMethodCallHandler { call, result ->
            if (call.method == "showToast") {
                val message = call.argument<String>("message")
                if (message != null) {
                    KToasty.info(this, message, Toast.LENGTH_SHORT, true).show()
                }
                result.success(null)
            } else {
                result.notImplemented()
            }
        }
    }
}
