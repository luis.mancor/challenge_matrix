import 'package:challenge_csti/core/utils/utility.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

part 'matrix_state.dart';
part 'matrix_event.dart';

class MatrixBloc extends Bloc<MatrixEvent, MatrixState> {
  MatrixBloc() : super(MatrixInitialState()) {
    on<InitialMatrixEvent>((event, emit) => emit(MatrixInitialState()));
    on<LoadingMatrixEvent>(_onLoadingChanged);
    on<ErrorMatrixEvent>(
        (event, emit) => emit(MatrixErrorState(message: event.message)));
  }

  void _onLoadingChanged(
      LoadingMatrixEvent event, Emitter<MatrixState> emit) async {
    try {
      emit(MatrixLoadingState());
      final List<List<int>> matrix = Utility.parseMatrix(event.matrix);
      int N = matrix.length;
      List<List<int>> rotatedMatrix =
          List.generate(N, (_) => List.filled(N, 0));
      for (int i = 0; i < N; i++) {
        for (int j = 0; j < N; j++) {
          rotatedMatrix[N - 1 - j][i] = matrix[i][j];
        }
      }
      await Future.delayed(const Duration(microseconds: 100));
      emit(MatrixLoadedState(matrix: rotatedMatrix, previousMatrix: matrix));
    } catch (e) {
      emit(MatrixErrorState(message: e.toString()));
    }
  }
}
