import 'dart:convert';

import '../../domain/entities/matrix.dart';

class MatrixModel extends Matrix {
  const MatrixModel({required super.matrix});

  factory MatrixModel.fromJson(String source) =>
      MatrixModel.fromMap(json.decode(source) as Map<String, dynamic>);

  factory MatrixModel.fromMap(Map<String, dynamic> map) {
    return MatrixModel(
      matrix: List<List<int>>.from(
        map['matrix']?.map((x) => List<int>.from(x)) ?? [],
      ),
    );
  }

  String toJson() => json.encode(toMap());

  Map<String, dynamic> toMap() {
    return {
      'matrix': matrix,
    };
  }

  MatrixModel copyWith({
    List<List<int>>? matrix,
  }) {
    return MatrixModel(
      matrix: matrix ?? this.matrix,
    );
  }
}
