import 'dart:convert';

import 'package:challenge_csti/core/constants.dart';
import 'package:challenge_csti/core/exceptions.dart';

class Utility {
  static List<List<int>> parseMatrix(String input) {
    try {
      if (!isValidBracketString(input)) {
        throw InvalidMatrixFormatException(Constants.errorMessageInvalidFormat);
      }
      var parsedData = jsonDecode(input);
      if (parsedData is! List) {
        throw InvalidListException(Constants.errorMessageNotAList);
      }
      List<List<int>> matrix = [];
      for (var row in parsedData) {
        if (row is! List || !row.every((element) => element is int)) {
          throw InvalidContentListException(
              Constants.errorMessageInvalidListContent);
        }
        matrix.add(List<int>.from(row));
      }
      int N = matrix.length;
      if (!matrix.every((row) => row.length == N)) {
        throw NotSquareMatrixException(Constants.errorMessageNotSquareMatrix);
      }
      return matrix;
    } on FormatException {
      throw InvalidMatrixFormatException(Constants.errorMessageInvalidFormat);
    } on InvalidContentListException {
      throw InvalidContentListException(
          Constants.errorMessageInvalidListContent);
    } on InvalidMatrixFormatException {
      throw InvalidMatrixFormatException(Constants.errorMessageInvalidFormat);
    } on InvalidListException {
      throw InvalidListException(Constants.errorMessageNotAList);
    } on NotSquareMatrixException {
      throw NotSquareMatrixException(Constants.errorMessageNotSquareMatrix);
    }
  }

  static bool isValidBracketString(String input) {
    int openBracketCount = 0;
    for (int i = 0; i < input.length; i++) {
      if (input[i] == '[') {
        openBracketCount++;
      } else if (input[i] == ']') {
        if (openBracketCount == 0) {
          return false;
        }
        openBracketCount--;
      }
    }
    return openBracketCount == 0;
  }
}
